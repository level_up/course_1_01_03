package org.levelup.lesson1;

public class HelloWorld {

    public static void main(String[] args) {
        // Выввод на экран строки Hello world!
        System.out.println("Hello world!");

        int a; // объявили переменную
        a = 15; // присвоили значением 15

        int b = 78; // инициализация переменной

        int c;
        c = a + b;
        System.out.println(c);

        double real = 434.23;
        long longValue = 4858489495958L;
        float floatValue = 54.34f;

        // если один из типов double -> double, иначе
        // если один из типов float -> float, иначе
        // если один из типов long -> long, иначе
        // будет int

        int result = (int) (77L / b);
        System.out.println("Result = " + result);

        byte bytes = (byte) longValue;
        long casted = bytes;

        int first = 5;
        int second = 27;
        int res = second % first;
        System.out.println("Result of mod = " + res);

        int value = 10;
        value++; // increment value = value + 1
        value--; // decrement value = value - 1
        ++value;
        --value;

        // 10
        System.out.println(value++);
        // print value
        // value = value + 1

        // value = value + 1
        // print value
        System.out.println(++value);

    }

}
