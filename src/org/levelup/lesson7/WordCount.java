package org.levelup.lesson7;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class WordCount {

    public static void main(String[] args) {
        String words = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n" +
                "\n" +
                "Why do we use it?\n" +
                "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\n" +
                "\n" +
                "\n" +
                "Where does it come from?\n" +
                "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem ";

        words = words.replace(",", "")
                .replace(".", "")
                .replace("!", "")
                .replace("?", "")
                .replace("\"", "")
                .replace("\n", " ");

        String[] allWords = words.split(" ");

        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < allWords.length; i++ ) {
            if (allWords[i] == null || allWords[i].trim().isEmpty()) {
                continue; // Don't use continue!
            }
            String w = allWords[i].toLowerCase();
            Integer count = map.get(w);
            if (count == null) {
                map.put(w, 1);
            } else {
                map.put(w, count + 1);
            }
        }

        Set<String> keys = map.keySet();
        keys.forEach(System.out::println);

        // for (<Type> var : collection)
        for (String key : keys) {
            Integer count = map.get(key);
            System.out.println(key + " " + count);
        }

        System.out.println();

        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        map.forEach((key, value) -> System.out.println(key + " " + value));

    }

}
