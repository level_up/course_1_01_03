package org.levelup.lesson7;

import java.util.*;

public class CollectionExample {

    public static void main(String[] args) {

        List<String> list = new LinkedList<>();
        list.add("String");
        list.add("s1");
        list.add("pull");
        list.add("push");

        System.out.println(list.toString());
        System.out.println("Index of pull: " + list.indexOf("pull"));
        System.out.println("Index of s2: " + list.indexOf("s2"));

        List<String> subList = list.subList(1, 2);
        System.out.println(subList.toString());

        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String el = iterator.next();
            if (el.length() > 3) {
                iterator.remove();
            }
        }

        System.out.println(list.toString());

    }

}
