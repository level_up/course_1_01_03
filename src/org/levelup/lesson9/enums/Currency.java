package org.levelup.lesson9.enums;

import java.util.Iterator;

// 1. Можно ли наследовать enum от другого enum Нет
// 2. Можно ли наследовать enum от другого класса Нет
// 3. Можно ли наследовать класс от enum Нет
// 4. Может ли enum реализовывать интерфейс Да
// 5. Можно ли наследовать enum от абстрактного класса Нет
// 6. Может ли enum иметь абстрактный метод Да
public enum Currency implements Iterable {

    EUR("Евро") {
        @Override
        public double convert() {
            return 0;
        }

        @Override
        public Iterator iterator() {
            return null;
        }
    },
    DOLLAR("Доллар") {
        @Override
        public double convert() {
            return 0;
        }

        @Override
        public Iterator iterator() {
            return null;
        }
    },
    RUB("Рубль") {
        @Override
        public double convert() {
            return 0;
        }

        @Override
        public Iterator iterator() {
            return null;
        }
    };

    private String russianName;

    Currency(String russianName) {
        this.russianName = russianName;
    }

    public String getRussianName() {
        return russianName;
    }

    public abstract double convert();

}
