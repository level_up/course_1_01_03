package org.levelup.lesson9.enums;

import java.util.Arrays;

public class EnumExample {

    public static void main(String[] args) {
        Currency currency = Currency.EUR;
        Currency dollar = Currency.DOLLAR;
        Currency rub = Currency.RUB;

        System.out.println(currency.name());
        System.out.println(currency.ordinal());

        Currency fromString = Currency.valueOf("DOLLAR");
        System.out.println(fromString == dollar);

        Currency[] values = Currency.values();
        System.out.println(Arrays.toString(values));

        System.out.println(rub.getRussianName());

    }

}
