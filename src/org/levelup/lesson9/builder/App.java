package org.levelup.lesson9.builder;

import org.levelup.lesson9.enums.Currency;

import java.util.Collection;

public class App {

    public static void main(String[] args) {

        Country country = new Country.CountryBuilder()
                .withName("Russia")
                .withCapital("Moscow")
                .withPeopleCount(143_500_000)
                .withVVP(4569)
                .withSquare(9_851_659)
                .withCurrency(Currency.RUB)
                .build();

        System.out.println(country.toString());

        Country usa = Country.builder()
                .withName("USA")
                .withCapital("Washington DC")
                .withPeopleCount(300_432_123)
                .withVVP(43243)
                .withSquare(2_423_233)
                .withCurrency(Currency.DOLLAR)
                .build();

        System.out.println(usa.toString());
    }

}
