package org.levelup.lesson9;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class StreamApi {

    public static void main(String[] args) {
        Collection<String> countries = new ArrayList<>();
        countries.add("Russia");
        countries.add("USA");
        countries.add("Canada");
        countries.add("England");
        countries.add("China");
        countries.add("Spain");

//        countries.forEach(new Consumer<String>() {
//            @Override
//            public void accept(String s) {
//
//            }
//        });
        for (String country : countries) {
            System.out.println(country);
        }

        countries.hashCode();

        countries.forEach( (country) -> {
            System.out.println(country);
        });

        countries.forEach(country -> System.out.println(country));

        countries.forEach(System.out::println);

        //
//        Collection<String> filtered = new ArrayList<>();
//        for (String country : countries) {
//            if (country.length() > 5) {
//                filtered.add(country);
//            }
//        }

        Collection<String> filtered  = countries.stream()
//                .filter(country -> {
//                    int length = country.length();
//                    return length > 5;
//                })
                .filter(country -> country.length() > 5)
                .collect(Collectors.toList());
        System.out.println(filtered);

        List<Integer> lengths = countries.stream()
//                .map(country -> country.length())
                .map(String::length)
                .collect(Collectors.toList());

        int[] values = new int[] { 3, 5, 6, 8, 9, 12 };
        int[] squares = Arrays.stream(values)
                .map(value -> value * value)
                .toArray();
        System.out.println(Arrays.toString(squares));

        Map<Integer, String> map = new HashMap<>();
        map.forEach((key, value) -> {
            System.out.println(key);
            System.out.println(value);
        });

        map.entrySet().stream()
                .filter(entry -> entry.getKey() > 5)
//                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()));
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        Map<Integer, List<String>> group = countries.stream()
                .collect(Collectors.groupingBy(String::length));
        group.entrySet().forEach(entry -> System.out.println(entry.getKey() + " " + entry.getValue().toString()));

    }

}
