package org.levelup.lesson5.person;

// Смена - сменный график работы
public interface Shiftable {

    public static final int START_WORK = 10;
    int END_WORK = 18;

    boolean isAtWork(int hour);

}
