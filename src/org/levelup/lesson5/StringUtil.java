package org.levelup.lesson5;

public class StringUtil {

    // true - value == null, value == "", value == "    "
    // trim() -> " asdf    ".trim() = "asdf"
    // "         " -> ""
    // "  as  as  " ->"as  as"
    public static boolean isEmpty(String value) {
//        if (value == null) return true;
//        return value.trim().isEmpty();
        return value == null || value.trim().isEmpty();
    }

    public static void main(String[] args) {
        System.out.println(isEmpty(null));
        System.out.println(isEmpty(""));
        System.out.println(isEmpty("        "));
        System.out.println(isEmpty("Some text"));
    }

}
