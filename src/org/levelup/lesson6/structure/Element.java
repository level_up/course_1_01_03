package org.levelup.lesson6.structure;

public class Element<TYPE> {

    private TYPE value;
    private Element next;

    public Element(TYPE value) {
        this.value = value;
    }

    public TYPE getValue() {
        return value;
    }

    public void setNext(Element next) {
        this.next = next;
    }

    public Element getNext() {
        return next;
    }
}
