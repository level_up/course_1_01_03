package org.levelup.lesson6.structure;

// Однонаправленный связный список
// One way list
public class List<TYPE>  {

    private int size;
    private Element<TYPE> head;

//    @Override
    public void add(TYPE value) {
        // 1. Проверить head == null.
        // 2-1. Если head = null -> записываем элемент в head
        // 2-2. Если head != null -> ищем последний элемент (у
        //                          которого next = null

        // Как ищем?
        // Цикл while, пока next != null
        Element<TYPE> el = new Element<>(value);
        if (head == null) {
            head = el;
        } else {
            Element current = head;
            while (current.getNext() != null) {
                current = current.getNext();
            }
            current.setNext(el);
        }
        size++;
    }

//    @Override
    public boolean isEmpty() {
        return false;
    }

//    @Override
    public int size() {
        return 0;
    }
}
