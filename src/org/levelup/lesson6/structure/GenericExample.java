package org.levelup.lesson6.structure;

public class GenericExample {

    public static void main(String[] args) {
        GenericClass<String> stringGenericClass = new GenericClass<>();
        stringGenericClass.getValue();

        GenericClass<Integer> integerGenericClass = new GenericClass<>();
        integerGenericClass.setValue(15);
//        integerGenericClass.setValue("");

        GenericClass<Object> objectGenericClass = new GenericClass<>();
        objectGenericClass.setValue(1);
        objectGenericClass.setValue("");
        objectGenericClass.setValue(new Object());

        // raw type
        GenericClass rawGenericClass = new GenericClass();
        rawGenericClass.setValue("");
    }

}
