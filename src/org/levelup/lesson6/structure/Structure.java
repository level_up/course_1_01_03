package org.levelup.lesson6.structure;

// int
public interface Structure {

    void add(int value);

    boolean isEmpty();

    int size();

}
