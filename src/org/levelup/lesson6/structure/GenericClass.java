package org.levelup.lesson6.structure;

public class GenericClass<TYPE> {

//    int v;
    TYPE value;

    public void setValue(TYPE value) {

        this.value = value;
    }

    public TYPE getValue() {
        return value;
    }
}
