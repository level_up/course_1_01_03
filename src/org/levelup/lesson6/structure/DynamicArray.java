package org.levelup.lesson6.structure;

// Динамический массив, список на основе массива
public class DynamicArray implements Structure {

    private int[] array;
    private int size; // Количество элементов в структуре

    public DynamicArray(int initialCapacity) {
        if (initialCapacity <= 0) {
            initialCapacity = 10;
        }
        this.array = new int[initialCapacity];
    }

    @Override
    public void add(int value) {
        // 1. Проверяем, если место куда вставить
        // 2-1. Если место есть: запихиваем данные
        // 2-2. Если места нет:
        //      3. Создаем новый массив большего размера (в 1.5 раза)
        //      4. Копируем старый в новый
        // Увеличиваем size
        if (array.length == size) {
            int[] oldArray = array;
            array = new int[(int)(oldArray.length * 1.5)];
            System.arraycopy(oldArray, 0, array, 0, size);
        }
        array[size++] = value;
//        size = size + 1; size++; size += 1;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }
}
