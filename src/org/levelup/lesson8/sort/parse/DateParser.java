package org.levelup.lesson8.sort.parse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParser {

    // dd.MM.yyyy
    public Date parse(String value) throws ParseException {
        // 02.04.2019
        // d - day
        // M - month
        // yyyy - year
        // h - 12 (am, pm)
        // H - 24
        // m - minute
        // s - seconds
        // S - millis
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        // try-catch-finally
//        try {
//            return formatter.parse(value);
//        } catch (ParseException exc) {
//            System.out.println("Invalid date");
//            return null;
//        }
        return formatter.parse(value);
    }

}
