package org.levelup.lesson8.sort.parse;

import java.text.ParseException;
import java.util.Date;

public class App {

    public static void main(String[] args) throws ParseException {
        Date now = new Date();
        System.out.println(now.getTime());
        System.out.println(now.toString());

        DateParser parser = new DateParser();
        Date parsedDate = parser.parse("02.05.2019");
        System.out.println(parsedDate);

    }

}
