package org.levelup.lesson8.sort;

public class TryCatchFinally {

    public static void main(String[] args) {
        int result = whatIsIt();
        System.out.println(result);
    }

    private static int whatIsIt() {
        try {
            return 1;
//            throw new Exception();
        } catch (Exception exc) {
            return 2;
        } finally {
            return 3;
        }
    }

}
