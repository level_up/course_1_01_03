package org.levelup.lesson8.sort;

public class StackExample {

    public static void main(String[] args) {
        Stack stack = new Stack();

        stack.push();
        stack.push();
        stack.push();
        stack.push();
        stack.push();
        stack.push();
        stack.push();
        stack.push();
        stack.push();
//        stack.push();
//        stack.push();

        try {
            stack.pop();
            stack.pop();
            stack.pop();
            stack.pop();
            stack.pop();
            stack.pop();
            stack.pop();
            stack.pop();
            stack.pop();
//            stack.pop();
//            stack.pop();
//            stack.pop();
//            stack.pop();
        } catch (EmptyStackException exc) {
            exc.printStackTrace();
        } finally {
            System.out.println("Finally");
        }

    }

}
