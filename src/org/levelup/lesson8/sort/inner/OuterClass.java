package org.levelup.lesson8.sort.inner;

public class OuterClass {

    private int field;

    // Внутренний класс
    public class InnerClass {

        public int getAndIncrement() {
            return field++;
        }

    }

    private static void printField() {
        System.out.println("Static method");
    }

    // Вложенный класс
    public static class NestedClass {

        public void print() {
//            field;
            printField();
        }

    }

}
