package org.levelup.lesson8.sort.inner;

import java.util.Comparator;

public class Application {

    public static void main(String[] args) {
//        new OuterClass().new InnerClass();
        OuterClass outer = new OuterClass();
        OuterClass.InnerClass inner = outer.new InnerClass();
        inner.getAndIncrement();
        int increment = inner.getAndIncrement();
        System.out.println(increment);

        OuterClass.NestedClass nested = new OuterClass.NestedClass();
        nested.print();

        // Compute ....
        class LocalClass {
            int field;
            double doubleField;
        }

        LocalClass localClass = new LocalClass();
        localClass.doubleField = 12.3;
        localClass.field = 32;

        Comparator<String> reverseStringComparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return -o1.compareTo(o2);
            }
        };


    }

}
