package org.levelup.lesson8.sort;

public class Stack {

    private int count;

    public void push() {
        count++;
        if (count == 10) {
            throw new StackOverflowException();
        }
    }

    public int pop() throws EmptyStackException {
        if (count == 0) {
            throw new EmptyStackException();
        }
        return --count;
    }

}
