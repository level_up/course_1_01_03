package org.levelup.lesson8.sort;

import java.util.Comparator;

public interface Sort {

    Comparable sort(Comparable[] comparables);

    <T extends Comparable<T>> T[] sortComparables(T[] objects);

    int[] sort(int[] objects, Comparator comparator);

}
