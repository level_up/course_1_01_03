package org.levelup.lesson10;

import org.bouncycastle.crypto.signers.StandardDSAEncoding;

import java.io.*;
import java.nio.charset.Charset;

public class FileReaderExample {

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("text.txt");

        // InputStream
        // Reader

        // OutputStream
        // Writer

        BufferedReader buffReader = null;
        try {
            buffReader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = buffReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException exc) {
            exc.printStackTrace();
        } finally {
            if (buffReader != null) {
                try {
                    buffReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // try-with-resources
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            String word = "Java";
            while ((line = reader.readLine()) != null) {
                int index = line.indexOf(word);
                if (index > -1) {
                    System.out.println(line.substring(index, index + word.length()));
                }

                System.out.println(line);
            }
        } catch (IOException exc) {
            exc.printStackTrace();
        }

        // Read from console
        BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
        // Set charset
        new BufferedReader(new InputStreamReader(new FileInputStream(file), Charset.forName("cp1251")));

        StandardDSAEncoding standardDSAEncoding = new StandardDSAEncoding();

    }

}
