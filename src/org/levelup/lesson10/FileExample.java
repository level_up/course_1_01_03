package org.levelup.lesson10;

import java.io.File;
import java.io.IOException;

@SuppressWarnings("all")
public class FileExample {

    public static void main(String[] args) throws IOException {
        File file = new File("text.txt");
        File another = new File("text2.txt");
        System.out.println(file == null);
        System.out.println(another == null);

        System.out.println("Is exist: " + another.exists());
        boolean result = another.createNewFile();
        System.out.println("Is created: " + result);

        System.out.println("Absolute path: " + another.getAbsolutePath());

        File dir = new File("src/");
        System.out.println("Is directory: " + dir.isDirectory());

        File anotherDir = new File("test/lesson1/test/create/dir/test");
        boolean isCreated = anotherDir.mkdirs();
        System.out.println("Is directory created: " + isCreated);

        File newFile = new File("test/lesson1/test/qwerty.txt");
        newFile.createNewFile();

        File outside = new File("../../Outside.txt");
        outside.createNewFile();
        outside.renameTo(new File("Outside.txt"));

    }

}
