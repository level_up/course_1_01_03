package org.levelup.lesson10;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@SuppressWarnings("all")
public class FileWriterExample {

    public static void main(String[] args) throws IOException {

        File file = new File("text3.txt");
        file.createNewFile();

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
            for (int i = 0; i < 10; i++) {
                writer.write("We love Java!\n");

                // batch
//                writer.flush();
            }
            writer.write("qwertyasdfghzxcvbn", 3, 5);

        }

    }

}
