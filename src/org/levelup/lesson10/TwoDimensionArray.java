package org.levelup.lesson10;

import java.io.*;
import java.util.StringTokenizer;

public class TwoDimensionArray {

    public static void main(String[] args) {

        try (BufferedReader reader = new BufferedReader(new FileReader(new File("two-dimension-array.txt")))) {

            String line;
            while ((line = reader.readLine()) != null) {
//                System.out.println(line);
//                StringTokenizer tokenizer = new StringTokenizer(line, "\t");
//                while (tokenizer.hasMoreElements()) {
//                    System.out.println(tokenizer.nextElement());
//                }
                String[] columns = line.split("\\s+");
                System.out.println(columns[1]);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        // JAXB

        // JSON
        // Jackson
        // Gson
        // JPath
        // simple-json


    }

}
