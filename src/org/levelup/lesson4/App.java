package org.levelup.lesson4;

import java.util.Arrays;

public class App {

    public static void main(String[] args) {
        Publication publication = new Publication(10);
        Book book = new Book("Альпина", 40);

        book.author = "Стивен Кови";
        publication.author = "Doug Lea";
        System.out.println("Publisher: " + book.publisher);

        System.out.println("Formatted name: " + book.getFormattedName());
        System.out.println("Publication formatted name: " + publication.getFormattedName());


        ElectronicBook electronicBook = new ElectronicBook();
        System.out.println("Electronic formatted name: " + electronicBook.getFormattedName());

        Publication[] publicationArray = new Publication[3];
        // publicationArray[0] = (Publication) book;
        publicationArray[0] = book;
        publicationArray[1] = publication;
        publicationArray[2] = electronicBook;

        System.out.println();
        for (int i = 0; i < publicationArray.length; i++) {
            System.out.println(publicationArray[i].getFormattedName());
        }

        System.out.println(book);
        // Print array
        System.out.println(Arrays.toString(publicationArray));
    }

}
