package org.levelup.lesson4;

public class Comparision {

    public static void main(String[] args) {
        Book first = new Book("Publisher", 20); // 589515
        Book second = new Book("Publisher", 20); // 8878
        Book third = first; // 589515

        System.out.println(first.equals(second));
        System.out.println(first.hashCode());
        System.out.println(second.hashCode());
//        System.out.println(first.equals(third));

    }

}
