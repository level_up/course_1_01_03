package org.levelup.lesson4;

public class ObjectCast {

    public static void main(String[] args) {
        // String o = (String) object;
        ElectronicBook ebook = new ElectronicBook();
        Object object = ebook;
        Publication publication = (Publication) object;
//        Newspaper newspaper = (Newspaper) publication;
    }

}
