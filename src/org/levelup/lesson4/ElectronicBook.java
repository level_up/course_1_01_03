package org.levelup.lesson4;

public class ElectronicBook extends Book {

    @Override
    public String getFormattedName() {
        return "EBook: " + author;
    }

    public void print() {}

}
