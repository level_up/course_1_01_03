package org.levelup.lesson4;

public class Publication {

    String author;
    int pages;
    String publisher;


    public Publication(int pages) {
//        super();
        System.out.println("Publication constructor");
        this.pages = pages;
        this.publisher = "Wiley";
    }

    public String getFormattedName() {
        return author + " - " + publisher;
    }

}
