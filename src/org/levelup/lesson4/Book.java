package org.levelup.lesson4;

import java.util.Objects;

// родитель, базовый класс, суперкласс - тот класс, от которого
// унаследовались
// потомок, ребенок, подкласс, subclass - тот класс, который наследуется
public class Book extends Publication {

    public Book(String publisher, int pages) {
        super(pages);
        System.out.println("Book constructor");
        super.publisher = publisher;
    }

    public Book() {
        super(0);
    }

    @Override
    public String getFormattedName() {
//        return super.getFormattedName();
        return publisher + ", " + author + " - " + pages + "pg";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        // instanceof
        // null instanceof Class -> false
        if (!(o instanceof Book)) return false;

        Book other = (Book) o;
        return pages == other.pages &&
                Objects.equals(publisher, other.publisher);
    }

    @Override
    public int hashCode() {
        // return Objects.hash(pages, publisher);
        int result = 17;
        result += result * 31 + pages;
        result += result * 31 + publisher.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", pages=" + pages +
                ", publisher='" + publisher + '\'' +
                '}';
    }
}
