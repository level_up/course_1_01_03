package org.levelup.lesson2;

public class ParseInt {

    public static void main(String[] args) {
        int value = Integer.parseInt("100110", 2);
        System.out.println(value);

        int fromHex = Integer.parseInt("a43e", 16);
        System.out.println(fromHex);
    }

}