package org.levelup.lesson2;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    // psvm
    public static void main(String[] args) {
        // sout
        Random rnd = new Random();
        int secretNumber = rnd.nextInt(7) + 3;

        boolean isGuessed = false;
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            System.out.println("Введите число:");

            while (!sc.hasNextInt()) {
                System.out.println("Введите число:");
                sc.next();
            }
            int number = sc.nextInt();
//        if (number == secretNumber) {
//            System.out.println("Вы угадали!");
//        } else {
//            // System.out.println("Вы не угадали! Число было: " + secretNumber);
//            if (number > secretNumber) {
//                System.out.println("Перелет");
//            } else {
//                System.out.println("Недолет");
//            }
//        }
            if (number == secretNumber) {
                System.out.println("Вы угадали!");
                isGuessed = true;
                break;
            } else if (number > secretNumber) {
                System.out.println("Перелет");
            } else {
                System.out.println("Недолет");
            }
        }

        if (!isGuessed) {
            System.out.println("Вы не угадали за 3 попытки. Число = " + secretNumber);
        }

    }

}
