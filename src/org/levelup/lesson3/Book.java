package org.levelup.lesson3;

// private
// default-package (private-package)
// protected
// public
public class Book {

    // поле класса, field
    double weight;
    String name;
    private double price;
    int discount;

    Book() {
        name = "Без название";
    }

    public Book(String bookName) {
        name = bookName;
    }

    Book(String name, double price) {
        this.name = name;
        this.price = price;
    }

    double calculatePrice() {
        double finalPrice = price - price * discount / 100;
//        return finalPrice;
//        return Math.abs(0.56d);
//        return price - price * discount / 100;
        return calculate(0);
    }

    // do()
    // do(int a)
    // do(double b)
    // do(int a, double b)
    // do(double b, int a)
    // Перегрузка (method overloading)
    double calculatePrice(int personDiscount) {
        return calculate(personDiscount);
//        return calculatePrice() - price * personDiscount / 100;
//        return price - price * discount / 100 - price * personDiscount / 100;
    }

    private double calculate(int personDiscount) {
        return price - price * discount / 100 - price * personDiscount / 100;
    }

    void print() {
        System.out.println("Name: " + name + ", price: " + price);
    }

    // getter
    public double getPrice() {
        return price;
    }

    public void setupPrice(double price) {
        if (price >= 0.0) {
            this.price = price;
        }
    }

}
