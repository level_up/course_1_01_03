package org.levelup.lesson3;

public class HasChanged {

    public static void main(String[] args) {
        int primitive = 1;
        Book book = new Book("", 2);
        primitive = changePrimitive(primitive);
        changeBookWeight(book);
        System.out.println(primitive);  // 1 || 10
        System.out.println(book.weight);    // 2 || 15
    }

    static int changePrimitive(int primitive) {
        primitive = 10;
        return primitive;
    }

    static void changeBookWeight(Book book) {
        book.weight = 15;
    }

}
